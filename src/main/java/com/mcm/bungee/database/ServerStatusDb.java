package com.mcm.bungee.database;

import com.mcm.bungee.Main;
import com.mcm.bungee.MySql;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ServerStatusDb {

    public static String getServerName(String ip) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("SELECT * FROM Server_status WHERE ip = ?");
            statement.setString(1, ip);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                return rs.getString("server_name");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        create(ip);
        return null;
    }

    public static void create(String ip) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("INSERT INTO Server_status(ip) VALUES (?)");
            statement.setString(1, ip);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateStatus(String ip, boolean status) {
        if (status == false) setOnlineOnDisable(ip);
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Server_status SET status = ? WHERE ip = ?");
            statement.setString(2, ip);
            statement.setBoolean(1, status);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateOnline(String ip, int online) {
        Main.plugin.getProxy().getScheduler().runAsync(Main.plugin, new Runnable() {
            @Override
            public void run() {
                try {
                    PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Server_status SET online = ? WHERE ip = ?");
                    statement.setString(2, ip);
                    statement.setInt(1, online);
                    statement.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void setOnlineOnDisable(String ip) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("UPDATE Server_status SET online = ? WHERE ip = ?");
            statement.setString(2, ip);
            statement.setInt(1, 0);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
