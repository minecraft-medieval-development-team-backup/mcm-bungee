package com.mcm.bungee.database;

import com.mcm.bungee.Main;
import com.mcm.bungee.RabbitMq;
import com.mcm.bungee.cache.Last24h;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.DeliverCallback;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RMqChatManager {

    public static String key = RabbitMq.key;

    public static void syncChat() {
        sync(Main.server_name);
    }

    private static void sync(String channel_name) {
        try {
            Connection connection = RabbitMq.factory.newConnection();
            Channel channel = connection.createChannel();

            channel.queueDeclare(channel_name, true, false, true, null);
            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                String message = new String(delivery.getBody(), "UTF-8");

                if (message.contains(key + "/player_entry=")) {
                    String[] split = message.split("=");
                    Last24h.entry(split[1]);
                }
            };
            channel.basicConsume(channel_name, true, deliverCallback, consumerTag -> {
            });
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }
}
