package com.mcm.bungee.commands;

import com.mcm.bungee.cache.LoginSystemStatus;
import com.mcm.bungee.database.LoginStatusDb;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class CommandUpdate extends Command {

    public CommandUpdate() {
        super("bgupdate");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) sender;

            // * Atualizations *
            LoginSystemStatus.get().setStatus(LoginStatusDb.getStatus());
            // * - *

            player.sendMessage(ChatColor.GREEN + " * BungeeCord atualizado com sucesso!");
        } else {
            LoginSystemStatus.get().setStatus(LoginStatusDb.getStatus());
            sender.sendMessage(ChatColor.GREEN + " * BungeeCord atualizado com sucesso!");
        }
    }
}
