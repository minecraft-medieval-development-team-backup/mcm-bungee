package com.mcm.bungee;

import com.mcm.bungee.cache.Last24h;
import com.mcm.bungee.database.RMqChatManager;
import com.mcm.bungee.database.ServerStatusDb;
import com.mcm.bungee.listeners.*;
import com.mcm.bungee.cache.LoginSystemStatus;
import com.mcm.bungee.commands.CommandUpdate;
import com.mcm.bungee.database.LoginStatusDb;

import java.util.ArrayList;

public class RegisterMain {

    public static void register() {
        if (Main.server_name == null) Main.server_name = ServerStatusDb.getServerName(Main.ip + ":25565");
        RMqChatManager.syncChat();
        Main.plugin.getProxy().registerChannel("BungeeCord");
        Main.plugin.getProxy().getPluginManager().registerCommand(Main.plugin, new CommandUpdate());
        Main.plugin.getProxy().getPluginManager().registerListener(Main.plugin, new ServerLoginEvents());
        Main.plugin.getProxy().getPluginManager().registerListener(Main.plugin, new PluginMessageEvents());
        Main.plugin.getProxy().getPluginManager().registerListener(Main.plugin, new ServerSwitchEvents());
        Main.plugin.getProxy().getPluginManager().registerListener(Main.plugin, new ServerDisconnectEvents());
        new LoginSystemStatus("server", LoginStatusDb.getStatus()).insert();
        ServerLoginEvents.list = new ArrayList<>();
        Last24h.task();
    }
}
