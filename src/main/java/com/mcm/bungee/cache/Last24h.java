package com.mcm.bungee.cache;

import com.google.common.collect.Iterables;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.mcm.bungee.Main;
import com.mcm.bungee.RabbitMq;
import com.mcm.bungee.enums.BungeeList;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class Last24h {

    private static ArrayList<Last24h> last24h;

    public static void entry(String uuid) {
        if (last24h == null) last24h = new ArrayList<>();
        if (get(uuid) != null && !last24h.contains(get(uuid))) last24h.add(new Last24h(uuid).insert());
        for (BungeeList bungee : BungeeList.values()) {
            if (!bungee.name().replaceAll("_", "-").equals(Main.server_name)) RabbitMq.send("bungee", RabbitMq.key + "/player_entry=" + uuid);
        }

        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("update_last24h");
        out.write(getLast24h());

        ProxiedPlayer player = Iterables.getFirst(ProxyServer.getInstance().getPlayers(), null);
        player.getServer().getInfo().sendData("BungeeCord", out.toByteArray());
    }

    public static void task() {
        Main.plugin.getProxy().getScheduler().schedule(Main.plugin, new Runnable() {
            @Override
            public void run() {
                if (last24h == null) return;
                for (Last24h data : last24h) {
                    Date difference = new Date();
                    difference.setTime(new Date().getTime() - data.getDate().getTime());
                    if (difference.getHours() > 24) {
                        cache.remove(data.getUUID());
                    }
                }
            }
        }, 1, 1, TimeUnit.HOURS);
    }

    public static int getLast24h() {
        return last24h.size();
    }


    /**
     *
     */
    private String uuid;
    private Date date;
    private static HashMap<String, Last24h> cache = new HashMap<>();

    public Last24h (String uuid) {
        this.uuid = uuid;
        this.date = new Date();
    }

    public Last24h insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static Last24h get(String uuid) {
        return cache.get(uuid);
    }

    public String getUUID() {
        return uuid;
    }

    public Date getDate() {
        return date;
    }
}
