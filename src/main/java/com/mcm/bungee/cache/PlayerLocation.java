package com.mcm.bungee.cache;

import com.mcm.bungee.Redis;

public class PlayerLocation {

    public PlayerLocation (String nick_name, String server_name) {
        Redis.localhost.set(nick_name + "location", server_name);
    }

    public static String get(String nick_name) {
        return Redis.localhost.get(nick_name + "location");
    }

    public static void delete(String nick_name) {
        Redis.localhost.del(nick_name + "location");
    }
}
