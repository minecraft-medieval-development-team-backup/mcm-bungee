package com.mcm.bungee;

import net.md_5.bungee.api.plugin.Plugin;

public class Main extends Plugin {

    public static Main instance;
    public static Plugin plugin;

    public static String ip = "localhost";
    public static String table = "mcmedieval";
    public static String user = "admin";
    public static String password = "r4TzoVQU";

    public static String server_name;

    @Override
    public void onEnable() {
        Main.plugin = this;
        Main.instance = this;
        MySql.connect();
        Redis.connect();
        RabbitMq.connect();
        RegisterMain.register();
    }

    @Override
    public void onDisable() {
        MySql.disconnect();
        Redis.disconnect();
    }
}
