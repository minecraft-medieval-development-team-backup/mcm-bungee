package com.mcm.bungee.listeners;

import com.mcm.bungee.cache.Last24h;
import com.mcm.bungee.cache.LoginSystemStatus;
import com.mcm.bungee.cache.PlayerLocation;
import com.mcm.bungee.utils.UUIDFetcher;
import com.mcm.bungee.database.OriginalLogged;
import net.md_5.bungee.api.event.PreLoginEvent;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import java.util.ArrayList;

public class ServerLoginEvents implements Listener {

    public static ArrayList<String> list;

    @EventHandler
    public void pending(PreLoginEvent event) {
        boolean loginStatus = LoginSystemStatus.get().getStatus();

        if (loginStatus == true) {
            if (UUIDFetcher.getUUID(event.getConnection().getName()) != null) {
                list.add(event.getConnection().getName());
            } else {
                event.getConnection().setOnlineMode(false);
            }
        }
    }

    @EventHandler
    public void serverConnect(ServerConnectedEvent event) {
        Last24h.entry(event.getPlayer().getUniqueId().toString());
        if (PlayerLocation.get(event.getPlayer().getName()) == null) new PlayerLocation(event.getPlayer().getName(), event.getServer().getInfo().getName());
        if (list.contains(event.getPlayer().getName())) {
            OriginalLogged.setLogged(event.getPlayer().getName());
            list.remove(event.getPlayer().getName());
        }
    }
}
