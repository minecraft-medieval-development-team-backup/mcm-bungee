package com.mcm.bungee.listeners;

import com.mcm.bungee.cache.PlayerLocation;
import com.google.common.collect.Iterables;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PluginMessageEvents implements Listener {

    @EventHandler
    public void onReceive(net.md_5.bungee.api.event.PluginMessageEvent event) {
        if (!event.getTag().equalsIgnoreCase("BungeeCord")) {
            return;
        }

        ByteArrayDataInput in = ByteStreams.newDataInput(event.getData());
        String subChannel = in.readUTF();

        if (subChannel.equals("shop_admin_create")) {
            ByteArrayDataOutput out = ByteStreams.newDataOutput();
            out.writeUTF("shop_admin_create");
            out.writeUTF(in.readUTF());

            ProxiedPlayer player = Iterables.getFirst(ProxyServer.getInstance().getPlayers(), null);
            player.getServer().getInfo().sendData("BungeeCord", out.toByteArray());
            return;
        }

        if (subChannel.equals("shop_admin_remove")) {
            ByteArrayDataOutput out = ByteStreams.newDataOutput();
            out.writeUTF("shop_admin_remove");
            out.writeUTF(in.readUTF());

            ProxiedPlayer player = Iterables.getFirst(ProxyServer.getInstance().getPlayers(), null);
            player.getServer().getInfo().sendData("BungeeCord", out.toByteArray());
            return;
        }

        if (subChannel.equals("update_fix_problem_economy_1")) {
            ByteArrayDataOutput out = ByteStreams.newDataOutput();
            out.writeUTF("update_fix_problem_economy_1");
            out.writeUTF(in.readUTF());

            ProxiedPlayer player = Iterables.getFirst(ProxyServer.getInstance().getPlayers(), null);
            player.getServer().getInfo().sendData("BungeeCord", out.toByteArray());
            return;
        }

        if (subChannel.equals("update_item_cache")) {
            ByteArrayDataOutput out = ByteStreams.newDataOutput();
            out.writeUTF("update_item_cache");
            out.writeUTF(in.readUTF());

            ProxiedPlayer player = Iterables.getFirst(ProxyServer.getInstance().getPlayers(), null);
            player.getServer().getInfo().sendData("BungeeCord", out.toByteArray());
            return;
        }
    }
}
