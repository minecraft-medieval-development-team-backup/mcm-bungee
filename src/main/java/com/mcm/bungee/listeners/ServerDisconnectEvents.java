package com.mcm.bungee.listeners;

import com.mcm.bungee.cache.PlayerLocation;
import net.md_5.bungee.api.event.ServerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class ServerDisconnectEvents implements Listener {

    @EventHandler
    public void onQuit(ServerDisconnectEvent event) {
        if (PlayerLocation.get(event.getPlayer().getName()) != null) PlayerLocation.delete(event.getPlayer().getName());
    }
}
