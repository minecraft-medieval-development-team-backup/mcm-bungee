package com.mcm.bungee.listeners;

import com.mcm.bungee.Main;
import com.mcm.bungee.cache.PlayerLocation;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerSwitchEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.concurrent.TimeUnit;

public class ServerSwitchEvents implements Listener {

    @EventHandler
    public void onSwitch(ServerSwitchEvent event) {
        ProxiedPlayer player = event.getPlayer();
        Main.plugin.getProxy().getScheduler().schedule(Main.plugin, () -> {
            if (PlayerLocation.get(event.getPlayer().getName()) == null) new PlayerLocation(player.getName(), event.getPlayer().getServer().getInfo().getName());
        }, 1, TimeUnit.SECONDS);
    }
}
